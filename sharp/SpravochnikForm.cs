﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace sharp
{
    public partial class SpravochnikForm : Form
    {
        public SpravochnikForm()
        {
            InitializeComponent();
        }

        private void OK_spravichnik_button_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            Close();
        }

        private void Cancel_spravichnik_button_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }
        public Dictionary<int, string> AbonentData
        {
            set
            {
                Name_comboBox.DataSource= value.ToArray();
                Name_comboBox.DisplayMember = "Value";
            }
        }
        public int AbonentID
        {
            get { return ((KeyValuePair<int, string>)Name_comboBox.SelectedItem).Key; }
        }

        public Dictionary<int, string> ContactData
        {
            set
            {
                Number_comboBox.DataSource = value.ToArray();
                Number_comboBox.DisplayMember = "Value";
            }
        }

        public int ContactID
        {
            get { return ((KeyValuePair<int, string>)Number_comboBox.SelectedItem).Key; }
        }
    }
}
