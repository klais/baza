﻿namespace sharp
{
    partial class KontaktForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Number_textBox = new System.Windows.Forms.TextBox();
            this.OK_kontakt_button = new System.Windows.Forms.Button();
            this.Cancel_kontakt_button = new System.Windows.Forms.Button();
            this.Type_textBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // Number_textBox
            // 
            this.Number_textBox.Location = new System.Drawing.Point(6, 14);
            this.Number_textBox.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Number_textBox.Name = "Number_textBox";
            this.Number_textBox.Size = new System.Drawing.Size(94, 20);
            this.Number_textBox.TabIndex = 0;
            // 
            // OK_kontakt_button
            // 
            this.OK_kontakt_button.Location = new System.Drawing.Point(6, 123);
            this.OK_kontakt_button.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.OK_kontakt_button.Name = "OK_kontakt_button";
            this.OK_kontakt_button.Size = new System.Drawing.Size(84, 27);
            this.OK_kontakt_button.TabIndex = 2;
            this.OK_kontakt_button.Text = "OK";
            this.OK_kontakt_button.UseVisualStyleBackColor = true;
            this.OK_kontakt_button.Click += new System.EventHandler(this.OK_kontakt_button_Click);
            // 
            // Cancel_kontakt_button
            // 
            this.Cancel_kontakt_button.Location = new System.Drawing.Point(92, 123);
            this.Cancel_kontakt_button.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Cancel_kontakt_button.Name = "Cancel_kontakt_button";
            this.Cancel_kontakt_button.Size = new System.Drawing.Size(90, 27);
            this.Cancel_kontakt_button.TabIndex = 4;
            this.Cancel_kontakt_button.Text = "Cancel";
            this.Cancel_kontakt_button.UseVisualStyleBackColor = true;
            this.Cancel_kontakt_button.Click += new System.EventHandler(this.Cancel_kontakt_button_Click);
            // 
            // Type_textBox
            // 
            this.Type_textBox.Location = new System.Drawing.Point(6, 45);
            this.Type_textBox.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Type_textBox.Name = "Type_textBox";
            this.Type_textBox.Size = new System.Drawing.Size(94, 20);
            this.Type_textBox.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(102, 14);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Номер";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(102, 47);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(26, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Тип";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(6, 70);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(94, 21);
            this.comboBox1.TabIndex = 8;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(105, 77);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(71, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Провайдеры";
            // 
            // KontaktForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(232, 161);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Type_textBox);
            this.Controls.Add(this.Cancel_kontakt_button);
            this.Controls.Add(this.OK_kontakt_button);
            this.Controls.Add(this.Number_textBox);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "KontaktForm";
            this.Text = "Kontakt";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button OK_kontakt_button;
        private System.Windows.Forms.Button Cancel_kontakt_button;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        public System.Windows.Forms.TextBox Number_textBox;
        public System.Windows.Forms.TextBox Type_textBox;
        public System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label3;
    }
}