﻿namespace sharp
{
    partial class AbonentForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Name_textBox = new System.Windows.Forms.TextBox();
            this.Surname_textBox = new System.Windows.Forms.TextBox();
            this.Patronymic_textBox = new System.Windows.Forms.TextBox();
            this.OK_abonent_button = new System.Windows.Forms.Button();
            this.Address_textBox = new System.Windows.Forms.TextBox();
            this.Comment_textBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.Cancel_abonent_button = new System.Windows.Forms.Button();
            this.Birthday_textBox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // Name_textBox
            // 
            this.Name_textBox.Location = new System.Drawing.Point(12, 12);
            this.Name_textBox.Name = "Name_textBox";
            this.Name_textBox.Size = new System.Drawing.Size(100, 20);
            this.Name_textBox.TabIndex = 0;
            // 
            // Surname_textBox
            // 
            this.Surname_textBox.Location = new System.Drawing.Point(12, 38);
            this.Surname_textBox.Name = "Surname_textBox";
            this.Surname_textBox.Size = new System.Drawing.Size(100, 20);
            this.Surname_textBox.TabIndex = 1;
            // 
            // Patronymic_textBox
            // 
            this.Patronymic_textBox.Location = new System.Drawing.Point(12, 64);
            this.Patronymic_textBox.Name = "Patronymic_textBox";
            this.Patronymic_textBox.Size = new System.Drawing.Size(100, 20);
            this.Patronymic_textBox.TabIndex = 2;
            // 
            // OK_abonent_button
            // 
            this.OK_abonent_button.Location = new System.Drawing.Point(12, 198);
            this.OK_abonent_button.Name = "OK_abonent_button";
            this.OK_abonent_button.Size = new System.Drawing.Size(75, 23);
            this.OK_abonent_button.TabIndex = 3;
            this.OK_abonent_button.Text = "OK";
            this.OK_abonent_button.UseVisualStyleBackColor = true;
            this.OK_abonent_button.Click += new System.EventHandler(this.OK_abonent_button_Click);
            // 
            // Address_textBox
            // 
            this.Address_textBox.Location = new System.Drawing.Point(12, 119);
            this.Address_textBox.Name = "Address_textBox";
            this.Address_textBox.Size = new System.Drawing.Size(100, 20);
            this.Address_textBox.TabIndex = 4;
            // 
            // Comment_textBox
            // 
            this.Comment_textBox.Location = new System.Drawing.Point(12, 145);
            this.Comment_textBox.Name = "Comment_textBox";
            this.Comment_textBox.Size = new System.Drawing.Size(100, 20);
            this.Comment_textBox.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(118, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Имя";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(118, 41);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Фамилия";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(118, 67);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(54, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Отчество";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(118, 122);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(38, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Адрес";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(119, 151);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(77, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "Комментарий";
            // 
            // Cancel_abonent_button
            // 
            this.Cancel_abonent_button.Location = new System.Drawing.Point(94, 198);
            this.Cancel_abonent_button.Name = "Cancel_abonent_button";
            this.Cancel_abonent_button.Size = new System.Drawing.Size(75, 23);
            this.Cancel_abonent_button.TabIndex = 11;
            this.Cancel_abonent_button.Text = "Cancel";
            this.Cancel_abonent_button.UseVisualStyleBackColor = true;
            this.Cancel_abonent_button.Click += new System.EventHandler(this.Cancel_abonent_button_Click);
            // 
            // Birthday_textBox
            // 
            this.Birthday_textBox.Location = new System.Drawing.Point(12, 90);
            this.Birthday_textBox.Name = "Birthday_textBox";
            this.Birthday_textBox.Size = new System.Drawing.Size(100, 20);
            this.Birthday_textBox.TabIndex = 12;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(119, 96);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(87, 13);
            this.label6.TabIndex = 13;
            this.label6.Text = "День рождения";
            // 
            // AbonentForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(221, 229);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.Birthday_textBox);
            this.Controls.Add(this.Cancel_abonent_button);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Comment_textBox);
            this.Controls.Add(this.Address_textBox);
            this.Controls.Add(this.OK_abonent_button);
            this.Controls.Add(this.Patronymic_textBox);
            this.Controls.Add(this.Surname_textBox);
            this.Controls.Add(this.Name_textBox);
            this.Name = "AbonentForm";
            this.Text = "Абонент";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button OK_abonent_button;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        public System.Windows.Forms.TextBox Name_textBox;
        public System.Windows.Forms.TextBox Surname_textBox;
        public System.Windows.Forms.TextBox Patronymic_textBox;
        public System.Windows.Forms.TextBox Address_textBox;
        public System.Windows.Forms.TextBox Comment_textBox;
        public System.Windows.Forms.Button Cancel_abonent_button;
        private System.Windows.Forms.Label label6;
        public System.Windows.Forms.TextBox Birthday_textBox;
    }
}