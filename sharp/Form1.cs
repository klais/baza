﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace sharp
{
    public partial class Form1 : Form
    {
        private string ConnectionString = global::sharp.Properties.Settings.Default.Database1ConnectionString;
        private const string AbonentTableName = "Ischencko_abonent";
        private const string ContactTableName = "ischencko_contact";
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            updateAbonentDGW();
            updateContactDGW();
            updatePROVDGW();
            updatePHONEDGW();
        }

       

        void updateAbonentDGW()
        {
            var request = "SELECT * FROM [" + AbonentTableName + "] WHERE surname + ' ' + name + ' ' +patronymic LIKE '%" + FIND_FIO_BOX.Text + "%'";
            var adapter = new SqlDataAdapter(request, ConnectionString);
            var abonentTable = new DataTable();
            adapter.Fill(abonentTable);
            tabl_abonent.DataSource = abonentTable;
            tabl_abonent.Columns["id"].Visible = false;
            tabl_abonent.Columns["name"].HeaderText = "Имя";
            tabl_abonent.Columns["surname"].HeaderText = "Фамилия";
            tabl_abonent.Columns["patronymic"].HeaderText = "Отчество";
            tabl_abonent.Columns["birthday"].HeaderText = "Дата Рождения";
            tabl_abonent.Columns["addressw"].HeaderText = "Адрес";
            tabl_abonent.Columns["comment"].HeaderText = "Комментраий";
        }
        void updateContactDGW()
        {
            var request = "SELECT * FROM [" + ContactTableName + "] WHERE phone LIKE '%" + FIND_PHONE_BOX.Text + "%'";
            var adapter = new SqlDataAdapter(request, ConnectionString);
            var kontakTable = new DataTable();
            adapter.Fill(kontakTable);
            tabl_kotnakt.DataSource = kontakTable;
            tabl_kotnakt.Columns["id"].Visible = false;
            tabl_kotnakt.Columns["provider_id"].Visible = false;
            tabl_kotnakt.Columns["phone"].HeaderText = "Телефон";
            tabl_kotnakt.Columns["type"].HeaderText = "Тип";

        }

        void updatePROVDGW()
        {
            var request = "SELECT * FROM Ischencko_provider";
            var adapter = new SqlDataAdapter(request, ConnectionString);
            var provTable = new DataTable();
            adapter.Fill(provTable);
            tabl_provayder.DataSource = provTable;
            tabl_provayder.Columns["id"].Visible = false;
            tabl_provayder.Columns["name"].HeaderText = "Имя";
            tabl_provayder.Columns["score"].HeaderText = "Рейтинг";
        }

        void updatePHONEDGW()
        {
            var request = @"SELECT *FROM Ischencko_abonent JOIN  Ischencko_abonent_has_cont ON Ischencko_abonent.id=Ischencko_abonent_has_cont.abonent_id 
            JOIN ischencko_contact ON ischencko_contact.id= Ischencko_abonent_has_cont.contact_id LEFT JOIN Ischencko_provider ON Ischencko_provider.id=ischencko_contact.provider_id ";
            List<string> filter = new List<string>();

            if (FIND_PHONE_BOX.Text != "")
            {
                filter.Add("ischencko_contact.phone LIKE+'%" + FIND_PHONE_BOX.Text + "%'");

            }

            if (FIND_FIO_BOX.Text != "")
            {
                filter.Add("Ischencko_abonent.surname+' ' +Ischencko_abonent.patronymic+' ' +Ischencko_abonent.name LIKE+'%" + FIND_FIO_BOX.Text + "%'");
            }

            if (filter.Capacity != 0)
            {
                request += " WHERE ";
                for (int i = 0; i < filter.Count; i++)
                {
                    if (i == filter.Count - 1) request += filter[i];
                    else request += filter[i] + " AND ";
                }
            }
            var adapter = new SqlDataAdapter(request, ConnectionString);
            var abonTable = new DataTable();
            adapter.Fill(abonTable);
            tabl_spravochnik.DataSource = abonTable;
            


            tabl_spravochnik.Columns["Id"].Visible = false;
            tabl_spravochnik.Columns["addressw"].Visible = false;
            tabl_spravochnik.Columns["birthday"].Visible = false;
            tabl_spravochnik.Columns["comment"].Visible = false;
            tabl_spravochnik.Columns["contact_id"].Visible = false;
            tabl_spravochnik.Columns["abonent_id"].Visible = false;

            tabl_spravochnik.Columns["type"].Visible = false;
            tabl_spravochnik.Columns["provider_id"].Visible = false;
            tabl_spravochnik.Columns["score"].Visible = false;
            tabl_spravochnik.Columns["name"].HeaderText = "Имя";
            tabl_spravochnik.Columns["surname"].HeaderText = "Фамилия";
            tabl_spravochnik.Columns["patronymic"].HeaderText = "Отчество";
            tabl_spravochnik.Columns["phone"].HeaderText = "Номер телефона";
            tabl_spravochnik.Columns["Id1"].Visible = false;
            tabl_spravochnik.Columns["Id2"].Visible = false;
            tabl_spravochnik.Columns["name1"].HeaderText = "Имя провайдера";
        }




        private void FIND_PHONE_BOX_TextChanged(object sender, EventArgs e)
        {
            updatePHONEDGW();
            updateContactDGW();
            updatePHONEDGW();
        }

        private void FIND_FIO_BOX_TextChanged(object sender, EventArgs e)
        {
            updatePHONEDGW();
            updateAbonentDGW();
            updatePHONEDGW();

        }

        private void Add_Click(object sender, EventArgs e)//кнопка добавления в абонентах  
        {
            var form = new AbonentForm();
            var res = form.ShowDialog();
            if (res == DialogResult.OK)
            {
                var name = form.Name_textBox.Text;
                var surname = form.Surname_textBox.Text; ;
                var patronymic = form.Patronymic_textBox.Text;
                var birthday = form.Birthday_textBox.Text;
                var comment = form.Comment_textBox.Text;
                var addressw = form.Address_textBox.Text;
                var connection = new SqlConnection(ConnectionString);
                connection.Open();
                var request = "INSERT INTO Ischencko_abonent (name, surname, patronymic, birthday, addressw, comment)" + "VALUES ('" + name + "', '" + surname + "', '"
                    + patronymic + "', '" + birthday + "', '" + addressw + "', '" + comment + "')";
                var command = new SqlCommand(request, connection);
                command.ExecuteNonQuery();
                connection.Close();
                updateAbonentDGW();
            }
        }

        private void Change_Click(object sender, EventArgs e)//кнопка изменения в абонентах  
        {
            var row = tabl_abonent.SelectedRows.Count > 0 ? tabl_abonent.SelectedRows[0] : null;
            if (row == null)
            {
                MessageBox.Show("Сначала выберите строчку!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            var form = new AbonentForm();
            form.Surname_textBox.Text = row.Cells["surname"].Value.ToString();
            form.Name_textBox.Text = row.Cells["name"].Value.ToString();
            form.Patronymic_textBox.Text = row.Cells["patronymic"].Value.ToString();
            form.Address_textBox.Text = row.Cells["addressw"].Value.ToString();
            form.Comment_textBox.Text = row.Cells["comment"].Value.ToString();
            form.Birthday_textBox.Text = row.Cells["birthday"].Value.ToString();
            var res = form.ShowDialog();
            if (res == DialogResult.OK)
            {
                var surname = form.Surname_textBox.Text;
                var name = form.Name_textBox.Text;
                var patronymic = form.Patronymic_textBox.Text;
                var comment = form.Comment_textBox.Text;
                var address = form.Address_textBox.Text;
                var birthday = form.Birthday_textBox.Text;
                var id = row.Cells["Id"].Value.ToString();

                var connection = new SqlConnection(ConnectionString);
                connection.Open();
                var request = "UPDATE Ischencko_abonent SET surname = '" + surname + "', name = '" + name + "', " +
                    "patronymic = '" + patronymic + "', addressw = '" + address + "', comment = '" + comment + "', birthday = '" + birthday + "' WHERE Id = " + id + ";";
                var command = new SqlCommand(request, connection);
                command.ExecuteNonQuery();
                connection.Close();
                updateAbonentDGW();
            }

        }

        private void Delete_Click(object sender, EventArgs e)//кнопка удаления в абонентах  
        {
            var row = tabl_abonent.SelectedRows.Count > 0 ? tabl_abonent.SelectedRows[0] : null;
            if (row == null)
            {
                MessageBox.Show("Выберите строку!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            var id = row.Cells["Id"].Value.ToString();
            var connection = new SqlConnection(ConnectionString);
            connection.Open();
            var request = "DELETE FROM Ischencko_abonent WHERE Id = " + id + ";";
            var command = new SqlCommand(request, connection);
            command.ExecuteNonQuery();
            connection.Close();
            updateAbonentDGW();
        }

        private void Add_kontakt_Click(object sender, EventArgs e)//кнопка добавления в контактах 
        {
            var form = new KontaktForm();
            {
                var getReq = "SELECT *FROM Ischencko_provider";
                var contactAdapter = new SqlDataAdapter(getReq, ConnectionString);
                var dict = new Dictionary<int, string>();
                var providerTbl = new DataTable();
                contactAdapter.Fill(providerTbl);

                foreach (DataRow row in providerTbl.Rows)
                {
                    dict.Add((int)row["Id"], row["name"].ToString());
                }
                form.ProviderData = dict;
            }
            var res = form.ShowDialog();
            if (res == DialogResult.OK)
            {
                var phone = form.Number_textBox.Text;
                var type = form.Type_textBox.Text;
                var connection = new SqlConnection(ConnectionString);
                connection.Open();
                var request = "INSERT INTO ischencko_contact (phone, type, provider_id) VALUES ('" + phone + "', '" + type + "', '6')";
                var command = new SqlCommand(request, connection);
                command.ExecuteNonQuery(); 
                connection.Close();
                updateContactDGW();
            }
        }

        private void Change_kontakt_Click(object sender, EventArgs e)//кнопка изменения в контактах 
        {
            var row = tabl_kotnakt.SelectedRows.Count > 0 ? tabl_kotnakt.SelectedRows[0] : null;
            if (row == null)
            {
                MessageBox.Show("Выберите строку!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            var form = new KontaktForm();
            form.Number_textBox.Text = row.Cells["phone"].Value.ToString();
            form.Type_textBox.Text = row.Cells["type"].Value.ToString();
            {
                var getReq = "SELECT *FROM Ischencko_provider";
                var contactAdapter = new SqlDataAdapter(getReq, ConnectionString);
                var dict = new Dictionary<int, string>();
                var providerTbl = new DataTable();
                contactAdapter.Fill(providerTbl);

                foreach (DataRow dbrow in providerTbl.Rows)
                {
                    dict.Add((int)dbrow["Id"], dbrow["name"].ToString());
                }
                form.ProviderData = dict;
            }
            form.ProviderID = (int)row.Cells["provider_id"].Value;
            var res = form.ShowDialog();
            if (res == DialogResult.OK)
            {
                var phone = form.Number_textBox.Text;
                var type = form.Type_textBox.Text;
                var id = row.Cells["Id"].Value.ToString();
                var provider_id = form.ProviderID;
                var connection = new SqlConnection(ConnectionString);
                connection.Open();
                var request = "UPDATE ischencko_contact SET phone = '" + phone + "', type = '" + type + "', " +
                    "provider_id = '6' WHERE Id = " + id + ";";
                var command = new SqlCommand(request, connection);
                command.ExecuteNonQuery();
                connection.Close();
                updateContactDGW();
            }
        }

        private void Delete_kontakt_Click(object sender, EventArgs e)//кнопка удаления в контактах 
        {
            var row = tabl_kotnakt.SelectedRows.Count > 0 ? tabl_kotnakt.SelectedRows[0] : null;
            if (row == null)
            {
                MessageBox.Show("Выберите строку!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            var id = row.Cells["Id"].Value.ToString();
            var connection = new SqlConnection(ConnectionString);
            connection.Open();
            var request = "DELETE FROM ischencko_contact WHERE Id = " + id + ";";
            var command = new SqlCommand(request, connection);
            command.ExecuteNonQuery(); 
            connection.Close();
            updateContactDGW();
        }

        private void Add_pprovider_Click(object sender, EventArgs e)//кнопка добавления в провайдере 
        {
            var form = new ProviderForm();
            var res = form.ShowDialog();
            if (res == DialogResult.OK)
            {
                var name = form.Tarif_provider.Text;
                var score = form.Score_provider.Text; ;

                var connection = new SqlConnection(ConnectionString);
                connection.Open();
                var request = "INSERT INTO Ischencko_provider (name, score) VALUES ('" + name + "', '" + score + "')";
                var command = new SqlCommand(request, connection);
                command.ExecuteNonQuery();
                connection.Close();
                updatePROVDGW();
            }
        }

        private void Change_provider_Click(object sender, EventArgs e)//кнопка изменения в провайдере 
        {
            var row = tabl_provayder.SelectedRows.Count > 0 ? tabl_provayder.SelectedRows[0] : null;
            if (row == null)
            {
                MessageBox.Show("Выберите строку!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            var form = new ProviderForm();
            form.Tarif_provider.Text = row.Cells["name"].Value.ToString();
            form.Score_provider.Text = row.Cells["score"].Value.ToString();
            var res = form.ShowDialog();
            if (res == DialogResult.OK)
            {
                var name = form.Tarif_provider.Text;
                var score = form.Score_provider.Text;
                var id = row.Cells["Id"].Value.ToString();
                var connection = new SqlConnection(ConnectionString);
                connection.Open();
                var request = "UPDATE Ischencko_provider SET name = '" + name + "', score = '" + score + "' WHERE Id = " + id + ";";
                var command = new SqlCommand(request, connection);
                command.ExecuteNonQuery();
                connection.Close();
                updatePROVDGW();
            }

        }

        private void Delete_provider_Click(object sender, EventArgs e)//кнопка удаления в провайдере
        {
            var row = tabl_provayder.SelectedRows.Count > 0 ? tabl_provayder.SelectedRows[0] : null;
            if (row == null)
            {
                MessageBox.Show("Выберите строку!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            var id = row.Cells["Id"].Value.ToString();
            var connection = new SqlConnection(ConnectionString);
            connection.Open();
            var request = "DELETE FROM Ischencko_provider WHERE Id = " + id + ";";
            var command = new SqlCommand(request, connection);
            command.ExecuteNonQuery(); 
            connection.Close();
            updatePROVDGW();
            updateContactDGW();
            updatePHONEDGW();
            updateAbonentDGW();
        }

        private void Add_Spravich_button_Click(object sender, EventArgs e)//кнопка добавления в справочнике 
        {
            var form = new SpravochnikForm();
            {
                var getReq = "SELECT Id, surname, name, patronymic FROM" + "[" + AbonentTableName + "]";
                var Adapter = new SqlDataAdapter(getReq, ConnectionString);
                var dict = new Dictionary<int, string>();
                var Tbl = new DataTable();
                Adapter.Fill(Tbl);
                foreach(DataRow row in Tbl.Rows)
                {
                    string setS = row["surname"].ToString() + " " + row["name"].ToString() + " " + row["patronymic"].ToString();
                    dict.Add((int)row["Id"], setS);
                }
                form.AbonentData = dict;
            }
            {
                var getReq = "SELECT Id, phone FROM" + "[" + ContactTableName + "]";
                var Adapter = new SqlDataAdapter(getReq, ConnectionString);
                var dict = new Dictionary<int, string>();
                var Tbl = new DataTable();
                Adapter.Fill(Tbl);
                foreach(DataRow row in Tbl.Rows)
                {
                    dict.Add((int)row["Id"], row["phone"].ToString());
                }
                form.ContactData = dict;
            }
            if (form.ShowDialog() == DialogResult.OK)
            {
                var conn = new SqlConnection(ConnectionString);
                conn.Open();

                var request = "INSERT INTO Ischencko_abonent_has_cont" + "(abonent_id, contact_id)" + " VALUES " + "('" + form.AbonentID + "', '" + form.ContactID + "')";
                var com = new SqlCommand(request, conn);
                com.ExecuteNonQuery();

                conn.Close();

                updatePHONEDGW();
            }

        }

        private void Change_Spravich_button_Click(object sender, EventArgs e)//кнопка изменения в справочнике
        {
            
            var row2 = tabl_spravochnik.SelectedRows.Count > 0 ? tabl_spravochnik.SelectedRows[0] : null;
            if (row2 == null)
            {
                MessageBox.Show("Сначала выберите строчку!!!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            var form = new SpravochnikForm();
            {
                var getReq = "SELECT Id, surname, name, patronymic FROM" + "[" + AbonentTableName + "]";
                var Adapter = new SqlDataAdapter(getReq, ConnectionString);
                var dict = new Dictionary<int, string>();
                var Tbl = new DataTable();
                Adapter.Fill(Tbl);

                foreach (DataRow row in Tbl.Rows)
                {
                    string setS = row["surname"].ToString() + " " + row["name"].ToString() + " " + row["patronymic"].ToString();
                    dict.Add((int)row["Id"], setS);
                }


                form.AbonentData = dict;
            }

            {
                var getReq = "SELECT Id, phone FROM" + "[" + ContactTableName + "]";
                var Adapter = new SqlDataAdapter(getReq, ConnectionString);
                var dict = new Dictionary<int, string>();
                var Tbl = new DataTable();
                Adapter.Fill(Tbl);

                foreach (DataRow row in Tbl.Rows)
                {
                    dict.Add((int)row["Id"], row["phone"].ToString());
                }


                form.ContactData = dict;
            }
            DataGridViewSelectedRowCollection Row = tabl_spravochnik.SelectedRows;

            var mas = tabl_spravochnik.SelectedRows;
            var Condidat = mas[0].Cells["Id"].FormattedValue.ToString();
            var Condidat2 = mas[0].Cells["Id1"].FormattedValue.ToString();

            var conn = new SqlConnection(ConnectionString);
            conn.Open();
            string str = "";
            var req = "SELECT surname FROM Ischencko_abonent WHERE Id = " + Condidat + "";
            var com = new SqlCommand(req, conn);
            var last = com.ExecuteScalar();
            str += last + " ";
            req = "SELECT name FROM Ischencko_abonent WHERE Id = " + Condidat + "";
            com = new SqlCommand(req, conn);
            last = com.ExecuteScalar();
            str += last + " ";
            req = "SELECT patronymic FROM Ischencko_abonent WHERE Id = " + Condidat + "";
            com = new SqlCommand(req, conn);
            last = com.ExecuteScalar();
            str += last;
            form.Name_comboBox.Text = str;

            req = "SELECT phone FROM ischencko_contact WHERE Id = " + Condidat2 + "";
            com = new SqlCommand(req, conn);
            last = com.ExecuteScalar();
            form.Number_comboBox.Text = last.ToString();

            conn.Close();

            if (form.ShowDialog() == DialogResult.OK)
            {
                conn = new SqlConnection(ConnectionString);
                conn.Open();

                var request = "UPDATE Ischencko_abonent_has_cont " + " SET contact_id='" + form.ContactID +
                    "', " + "abonent_id ='" + form.AbonentID + "' WHERE contact_id=" + Condidat2 +
                    " AND abonent_id=" + Condidat;
                com = new SqlCommand(request, conn);
                com.ExecuteNonQuery();

                conn.Close();


                updatePHONEDGW();
            }

        }

        private void Delete_Spravich_button_Click(object sender, EventArgs e)//кнопка удаления в справочнике
        {
            var row = tabl_spravochnik.SelectedRows.Count > 0 ? tabl_spravochnik.SelectedRows[0] : null;
            if (row == null)
            {
                MessageBox.Show(" Выберите строку!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            var num1 = row.Cells["Id"].Value.ToString();
            var num2 = row.Cells["Id1"].Value.ToString();
            var connection = new SqlConnection(ConnectionString);
            connection.Open();
            var request = "DELETE FROM Ischencko_abonent_has_cont WHERE contact_id = '" + num2 +
                "' AND abonent_id='" + num1 + "'";
            var command = new SqlCommand(request, connection);
            command.ExecuteNonQuery(); 
            connection.Close();
            updatePHONEDGW();
        }
    }
}
