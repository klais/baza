﻿namespace sharp
{
    partial class Form1
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.kontakt = new System.Windows.Forms.TabControl();
            this.abonent = new System.Windows.Forms.TabPage();
            this.Delete = new System.Windows.Forms.Button();
            this.Change = new System.Windows.Forms.Button();
            this.Add = new System.Windows.Forms.Button();
            this.tabl_abonent = new System.Windows.Forms.DataGridView();
            this.kontak = new System.Windows.Forms.TabPage();
            this.Delete_kontakt = new System.Windows.Forms.Button();
            this.Change_kontakt = new System.Windows.Forms.Button();
            this.Add_kontakt = new System.Windows.Forms.Button();
            this.tabl_kotnakt = new System.Windows.Forms.DataGridView();
            this.provayd = new System.Windows.Forms.TabPage();
            this.Delete_provider = new System.Windows.Forms.Button();
            this.Change_provider = new System.Windows.Forms.Button();
            this.Add_pprovider = new System.Windows.Forms.Button();
            this.tabl_provayder = new System.Windows.Forms.DataGridView();
            this.spravoch = new System.Windows.Forms.TabPage();
            this.Delete_Spravich_button = new System.Windows.Forms.Button();
            this.Change_Spravich_button = new System.Windows.Forms.Button();
            this.Add_Spravich_button = new System.Windows.Forms.Button();
            this.tabl_spravochnik = new System.Windows.Forms.DataGridView();
            this.FIND_PHONE_BOX = new System.Windows.Forms.TextBox();
            this.FIND_FIO_BOX = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.kontakt.SuspendLayout();
            this.abonent.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabl_abonent)).BeginInit();
            this.kontak.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabl_kotnakt)).BeginInit();
            this.provayd.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabl_provayder)).BeginInit();
            this.spravoch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabl_spravochnik)).BeginInit();
            this.SuspendLayout();
            // 
            // kontakt
            // 
            this.kontakt.Controls.Add(this.abonent);
            this.kontakt.Controls.Add(this.kontak);
            this.kontakt.Controls.Add(this.provayd);
            this.kontakt.Controls.Add(this.spravoch);
            this.kontakt.Location = new System.Drawing.Point(12, 26);
            this.kontakt.Name = "kontakt";
            this.kontakt.SelectedIndex = 0;
            this.kontakt.Size = new System.Drawing.Size(1013, 504);
            this.kontakt.TabIndex = 1;
            // 
            // abonent
            // 
            this.abonent.Controls.Add(this.Delete);
            this.abonent.Controls.Add(this.Change);
            this.abonent.Controls.Add(this.Add);
            this.abonent.Controls.Add(this.tabl_abonent);
            this.abonent.Location = new System.Drawing.Point(4, 22);
            this.abonent.Name = "abonent";
            this.abonent.Padding = new System.Windows.Forms.Padding(3);
            this.abonent.Size = new System.Drawing.Size(1005, 478);
            this.abonent.TabIndex = 0;
            this.abonent.Text = "Абонент";
            this.abonent.UseVisualStyleBackColor = true;
            // 
            // Delete
            // 
            this.Delete.Location = new System.Drawing.Point(336, 434);
            this.Delete.Name = "Delete";
            this.Delete.Size = new System.Drawing.Size(153, 23);
            this.Delete.TabIndex = 3;
            this.Delete.Text = "Удалить";
            this.Delete.UseVisualStyleBackColor = true;
            this.Delete.Click += new System.EventHandler(this.Delete_Click);
            // 
            // Change
            // 
            this.Change.Location = new System.Drawing.Point(159, 435);
            this.Change.Name = "Change";
            this.Change.Size = new System.Drawing.Size(171, 23);
            this.Change.TabIndex = 2;
            this.Change.Text = "Изменить ";
            this.Change.UseVisualStyleBackColor = true;
            this.Change.Click += new System.EventHandler(this.Change_Click);
            // 
            // Add
            // 
            this.Add.Location = new System.Drawing.Point(7, 435);
            this.Add.Name = "Add";
            this.Add.Size = new System.Drawing.Size(146, 23);
            this.Add.TabIndex = 1;
            this.Add.Text = "Добавить";
            this.Add.UseVisualStyleBackColor = true;
            this.Add.Click += new System.EventHandler(this.Add_Click);
            // 
            // tabl_abonent
            // 
            this.tabl_abonent.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tabl_abonent.Location = new System.Drawing.Point(0, 0);
            this.tabl_abonent.Name = "tabl_abonent";
            this.tabl_abonent.RowHeadersWidth = 82;
            this.tabl_abonent.Size = new System.Drawing.Size(999, 428);
            this.tabl_abonent.TabIndex = 0;
            // 
            // kontak
            // 
            this.kontak.Controls.Add(this.Delete_kontakt);
            this.kontak.Controls.Add(this.Change_kontakt);
            this.kontak.Controls.Add(this.Add_kontakt);
            this.kontak.Controls.Add(this.tabl_kotnakt);
            this.kontak.Location = new System.Drawing.Point(4, 22);
            this.kontak.Name = "kontak";
            this.kontak.Padding = new System.Windows.Forms.Padding(3);
            this.kontak.Size = new System.Drawing.Size(1005, 478);
            this.kontak.TabIndex = 1;
            this.kontak.Text = "Контакт";
            this.kontak.UseVisualStyleBackColor = true;
            // 
            // Delete_kontakt
            // 
            this.Delete_kontakt.Location = new System.Drawing.Point(270, 446);
            this.Delete_kontakt.Margin = new System.Windows.Forms.Padding(2);
            this.Delete_kontakt.Name = "Delete_kontakt";
            this.Delete_kontakt.Size = new System.Drawing.Size(126, 29);
            this.Delete_kontakt.TabIndex = 5;
            this.Delete_kontakt.Text = "Удалить";
            this.Delete_kontakt.UseVisualStyleBackColor = true;
            this.Delete_kontakt.Click += new System.EventHandler(this.Delete_kontakt_Click);
            // 
            // Change_kontakt
            // 
            this.Change_kontakt.Location = new System.Drawing.Point(134, 446);
            this.Change_kontakt.Margin = new System.Windows.Forms.Padding(2);
            this.Change_kontakt.Name = "Change_kontakt";
            this.Change_kontakt.Size = new System.Drawing.Size(134, 29);
            this.Change_kontakt.TabIndex = 3;
            this.Change_kontakt.Text = "Изменить";
            this.Change_kontakt.UseVisualStyleBackColor = true;
            this.Change_kontakt.Click += new System.EventHandler(this.Change_kontakt_Click);
            // 
            // Add_kontakt
            // 
            this.Add_kontakt.Location = new System.Drawing.Point(4, 446);
            this.Add_kontakt.Margin = new System.Windows.Forms.Padding(2);
            this.Add_kontakt.Name = "Add_kontakt";
            this.Add_kontakt.Size = new System.Drawing.Size(126, 29);
            this.Add_kontakt.TabIndex = 1;
            this.Add_kontakt.Text = "Добавить";
            this.Add_kontakt.UseVisualStyleBackColor = true;
            this.Add_kontakt.Click += new System.EventHandler(this.Add_kontakt_Click);
            // 
            // tabl_kotnakt
            // 
            this.tabl_kotnakt.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tabl_kotnakt.Location = new System.Drawing.Point(0, 0);
            this.tabl_kotnakt.Name = "tabl_kotnakt";
            this.tabl_kotnakt.RowHeadersWidth = 82;
            this.tabl_kotnakt.Size = new System.Drawing.Size(1005, 432);
            this.tabl_kotnakt.TabIndex = 0;
            // 
            // provayd
            // 
            this.provayd.Controls.Add(this.Delete_provider);
            this.provayd.Controls.Add(this.Change_provider);
            this.provayd.Controls.Add(this.Add_pprovider);
            this.provayd.Controls.Add(this.tabl_provayder);
            this.provayd.Location = new System.Drawing.Point(4, 22);
            this.provayd.Name = "provayd";
            this.provayd.Padding = new System.Windows.Forms.Padding(3);
            this.provayd.Size = new System.Drawing.Size(1005, 478);
            this.provayd.TabIndex = 2;
            this.provayd.Text = "Провайдеры";
            this.provayd.UseVisualStyleBackColor = true;
            // 
            // Delete_provider
            // 
            this.Delete_provider.Location = new System.Drawing.Point(271, 444);
            this.Delete_provider.Margin = new System.Windows.Forms.Padding(2);
            this.Delete_provider.Name = "Delete_provider";
            this.Delete_provider.Size = new System.Drawing.Size(126, 29);
            this.Delete_provider.TabIndex = 8;
            this.Delete_provider.Text = "Удалить";
            this.Delete_provider.UseVisualStyleBackColor = true;
            this.Delete_provider.Click += new System.EventHandler(this.Delete_provider_Click);
            // 
            // Change_provider
            // 
            this.Change_provider.Location = new System.Drawing.Point(135, 444);
            this.Change_provider.Margin = new System.Windows.Forms.Padding(2);
            this.Change_provider.Name = "Change_provider";
            this.Change_provider.Size = new System.Drawing.Size(134, 29);
            this.Change_provider.TabIndex = 7;
            this.Change_provider.Text = "Изменить";
            this.Change_provider.UseVisualStyleBackColor = true;
            this.Change_provider.Click += new System.EventHandler(this.Change_provider_Click);
            // 
            // Add_pprovider
            // 
            this.Add_pprovider.Location = new System.Drawing.Point(5, 444);
            this.Add_pprovider.Margin = new System.Windows.Forms.Padding(2);
            this.Add_pprovider.Name = "Add_pprovider";
            this.Add_pprovider.Size = new System.Drawing.Size(126, 29);
            this.Add_pprovider.TabIndex = 6;
            this.Add_pprovider.Text = "Добавить";
            this.Add_pprovider.UseVisualStyleBackColor = true;
            this.Add_pprovider.Click += new System.EventHandler(this.Add_pprovider_Click);
            // 
            // tabl_provayder
            // 
            this.tabl_provayder.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tabl_provayder.Location = new System.Drawing.Point(0, 0);
            this.tabl_provayder.Name = "tabl_provayder";
            this.tabl_provayder.RowHeadersWidth = 82;
            this.tabl_provayder.Size = new System.Drawing.Size(999, 439);
            this.tabl_provayder.TabIndex = 0;
            // 
            // spravoch
            // 
            this.spravoch.Controls.Add(this.Delete_Spravich_button);
            this.spravoch.Controls.Add(this.Change_Spravich_button);
            this.spravoch.Controls.Add(this.Add_Spravich_button);
            this.spravoch.Controls.Add(this.tabl_spravochnik);
            this.spravoch.Location = new System.Drawing.Point(4, 22);
            this.spravoch.Name = "spravoch";
            this.spravoch.Padding = new System.Windows.Forms.Padding(3);
            this.spravoch.Size = new System.Drawing.Size(1005, 478);
            this.spravoch.TabIndex = 3;
            this.spravoch.Text = "Тел.справ";
            this.spravoch.UseVisualStyleBackColor = true;
            // 
            // Delete_Spravich_button
            // 
            this.Delete_Spravich_button.Location = new System.Drawing.Point(334, 448);
            this.Delete_Spravich_button.Name = "Delete_Spravich_button";
            this.Delete_Spravich_button.Size = new System.Drawing.Size(153, 23);
            this.Delete_Spravich_button.TabIndex = 6;
            this.Delete_Spravich_button.Text = "Удалить";
            this.Delete_Spravich_button.UseVisualStyleBackColor = true;
            this.Delete_Spravich_button.Click += new System.EventHandler(this.Delete_Spravich_button_Click);
            // 
            // Change_Spravich_button
            // 
            this.Change_Spravich_button.Location = new System.Drawing.Point(157, 449);
            this.Change_Spravich_button.Name = "Change_Spravich_button";
            this.Change_Spravich_button.Size = new System.Drawing.Size(171, 23);
            this.Change_Spravich_button.TabIndex = 5;
            this.Change_Spravich_button.Text = "Изменить ";
            this.Change_Spravich_button.UseVisualStyleBackColor = true;
            this.Change_Spravich_button.Click += new System.EventHandler(this.Change_Spravich_button_Click);
            // 
            // Add_Spravich_button
            // 
            this.Add_Spravich_button.Location = new System.Drawing.Point(5, 449);
            this.Add_Spravich_button.Name = "Add_Spravich_button";
            this.Add_Spravich_button.Size = new System.Drawing.Size(146, 23);
            this.Add_Spravich_button.TabIndex = 4;
            this.Add_Spravich_button.Text = "Добавить";
            this.Add_Spravich_button.UseVisualStyleBackColor = true;
            this.Add_Spravich_button.Click += new System.EventHandler(this.Add_Spravich_button_Click);
            // 
            // tabl_spravochnik
            // 
            this.tabl_spravochnik.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tabl_spravochnik.Location = new System.Drawing.Point(0, 0);
            this.tabl_spravochnik.Name = "tabl_spravochnik";
            this.tabl_spravochnik.RowHeadersWidth = 82;
            this.tabl_spravochnik.Size = new System.Drawing.Size(999, 442);
            this.tabl_spravochnik.TabIndex = 0;
            // 
            // FIND_PHONE_BOX
            // 
            this.FIND_PHONE_BOX.Location = new System.Drawing.Point(425, 556);
            this.FIND_PHONE_BOX.Name = "FIND_PHONE_BOX";
            this.FIND_PHONE_BOX.Size = new System.Drawing.Size(100, 20);
            this.FIND_PHONE_BOX.TabIndex = 2;
            this.FIND_PHONE_BOX.TextChanged += new System.EventHandler(this.FIND_PHONE_BOX_TextChanged);
            // 
            // FIND_FIO_BOX
            // 
            this.FIND_FIO_BOX.Location = new System.Drawing.Point(717, 556);
            this.FIND_FIO_BOX.Name = "FIND_FIO_BOX";
            this.FIND_FIO_BOX.Size = new System.Drawing.Size(100, 20);
            this.FIND_FIO_BOX.TabIndex = 3;
            this.FIND_FIO_BOX.TextChanged += new System.EventHandler(this.FIND_FIO_BOX_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(356, 562);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Телефон";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(648, 561);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Ф.И.О";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1034, 599);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.FIND_FIO_BOX);
            this.Controls.Add(this.FIND_PHONE_BOX);
            this.Controls.Add(this.kontakt);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.kontakt.ResumeLayout(false);
            this.abonent.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tabl_abonent)).EndInit();
            this.kontak.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tabl_kotnakt)).EndInit();
            this.provayd.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tabl_provayder)).EndInit();
            this.spravoch.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tabl_spravochnik)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TabControl kontakt;
        private System.Windows.Forms.TabPage abonent;
        private System.Windows.Forms.TabPage kontak;
        private System.Windows.Forms.TabPage provayd;
        private System.Windows.Forms.TabPage spravoch;
        private System.Windows.Forms.DataGridView tabl_abonent;
        private System.Windows.Forms.DataGridView tabl_kotnakt;
        private System.Windows.Forms.DataGridView tabl_provayder;
        private System.Windows.Forms.DataGridView tabl_spravochnik;
        private System.Windows.Forms.TextBox FIND_PHONE_BOX;
        private System.Windows.Forms.TextBox FIND_FIO_BOX;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button Delete;
        private System.Windows.Forms.Button Change;
        private System.Windows.Forms.Button Add;
        private System.Windows.Forms.Button Delete_kontakt;
        private System.Windows.Forms.Button Change_kontakt;
        private System.Windows.Forms.Button Add_kontakt;
        private System.Windows.Forms.Button Delete_provider;
        private System.Windows.Forms.Button Change_provider;
        private System.Windows.Forms.Button Add_pprovider;
        private System.Windows.Forms.Button Delete_Spravich_button;
        private System.Windows.Forms.Button Change_Spravich_button;
        private System.Windows.Forms.Button Add_Spravich_button;
    }
}

