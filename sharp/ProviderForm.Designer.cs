﻿namespace sharp
{
    partial class ProviderForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.Score_provider = new System.Windows.Forms.TextBox();
            this.Cancel_provider = new System.Windows.Forms.Button();
            this.OK_provider = new System.Windows.Forms.Button();
            this.Tarif_provider = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(107, 44);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 13);
            this.label2.TabIndex = 13;
            this.label2.Text = "Рейтинг";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(107, 11);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 13);
            this.label1.TabIndex = 12;
            this.label1.Text = "Тариф";
            // 
            // Score_provider
            // 
            this.Score_provider.Location = new System.Drawing.Point(11, 42);
            this.Score_provider.Margin = new System.Windows.Forms.Padding(2);
            this.Score_provider.Name = "Score_provider";
            this.Score_provider.Size = new System.Drawing.Size(94, 20);
            this.Score_provider.TabIndex = 11;
            // 
            // Cancel_provider
            // 
            this.Cancel_provider.Location = new System.Drawing.Point(97, 90);
            this.Cancel_provider.Margin = new System.Windows.Forms.Padding(2);
            this.Cancel_provider.Name = "Cancel_provider";
            this.Cancel_provider.Size = new System.Drawing.Size(90, 27);
            this.Cancel_provider.TabIndex = 10;
            this.Cancel_provider.Text = "Cancel";
            this.Cancel_provider.UseVisualStyleBackColor = true;
            this.Cancel_provider.Click += new System.EventHandler(this.Cancel_provider_Click);
            // 
            // OK_provider
            // 
            this.OK_provider.Location = new System.Drawing.Point(11, 90);
            this.OK_provider.Margin = new System.Windows.Forms.Padding(2);
            this.OK_provider.Name = "OK_provider";
            this.OK_provider.Size = new System.Drawing.Size(84, 27);
            this.OK_provider.TabIndex = 9;
            this.OK_provider.Text = "OK";
            this.OK_provider.UseVisualStyleBackColor = true;
            this.OK_provider.Click += new System.EventHandler(this.OK_provider_Click);
            // 
            // Tarif_provider
            // 
            this.Tarif_provider.Location = new System.Drawing.Point(11, 11);
            this.Tarif_provider.Margin = new System.Windows.Forms.Padding(2);
            this.Tarif_provider.Name = "Tarif_provider";
            this.Tarif_provider.Size = new System.Drawing.Size(94, 20);
            this.Tarif_provider.TabIndex = 8;
            // 
            // ProviderForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(220, 124);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Score_provider);
            this.Controls.Add(this.Cancel_provider);
            this.Controls.Add(this.OK_provider);
            this.Controls.Add(this.Tarif_provider);
            this.Name = "ProviderForm";
            this.Text = "ProviderForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.TextBox Score_provider;
        private System.Windows.Forms.Button Cancel_provider;
        private System.Windows.Forms.Button OK_provider;
        public System.Windows.Forms.TextBox Tarif_provider;
    }
}