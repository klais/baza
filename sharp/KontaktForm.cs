﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace sharp
{
    public partial class KontaktForm : Form
    {
        public KontaktForm()
        {
            InitializeComponent();
        }
        private void OK_kontakt_button_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            Close();
        }
        private void Cancel_kontakt_button_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        public Dictionary<int, string> ProviderData
        {
            set
            {
                comboBox1.DataSource = value.ToArray();
                comboBox1.DisplayMember = "Value";
            }
        }

        public int ProviderID
        {
            get { return ((KeyValuePair<int, string>)comboBox1.SelectedItem).Key; }
            set
            {
                int idx = 0;
                foreach (KeyValuePair<int, string> item in comboBox1.Items)
                {
                    if (item.Key == value) break;
                    idx++;
                }
                comboBox1.SelectedIndex = idx;
            }
        }
    }
}
