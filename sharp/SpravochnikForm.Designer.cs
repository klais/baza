﻿namespace sharp
{
    partial class SpravochnikForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Name_comboBox = new System.Windows.Forms.ComboBox();
            this.Number_comboBox = new System.Windows.Forms.ComboBox();
            this.Cancel_spravichnik_button = new System.Windows.Forms.Button();
            this.OK_spravichnik_button = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // Name_comboBox
            // 
            this.Name_comboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Name_comboBox.FormattingEnabled = true;
            this.Name_comboBox.Location = new System.Drawing.Point(12, 12);
            this.Name_comboBox.Name = "Name_comboBox";
            this.Name_comboBox.Size = new System.Drawing.Size(176, 21);
            this.Name_comboBox.TabIndex = 0;
            // 
            // Number_comboBox
            // 
            this.Number_comboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Number_comboBox.FormattingEnabled = true;
            this.Number_comboBox.Location = new System.Drawing.Point(12, 65);
            this.Number_comboBox.Name = "Number_comboBox";
            this.Number_comboBox.Size = new System.Drawing.Size(176, 21);
            this.Number_comboBox.TabIndex = 1;
            // 
            // Cancel_spravichnik_button
            // 
            this.Cancel_spravichnik_button.Location = new System.Drawing.Point(98, 128);
            this.Cancel_spravichnik_button.Margin = new System.Windows.Forms.Padding(2);
            this.Cancel_spravichnik_button.Name = "Cancel_spravichnik_button";
            this.Cancel_spravichnik_button.Size = new System.Drawing.Size(90, 27);
            this.Cancel_spravichnik_button.TabIndex = 6;
            this.Cancel_spravichnik_button.Text = "Cancel";
            this.Cancel_spravichnik_button.UseVisualStyleBackColor = true;
            this.Cancel_spravichnik_button.Click += new System.EventHandler(this.Cancel_spravichnik_button_Click);
            // 
            // OK_spravichnik_button
            // 
            this.OK_spravichnik_button.Location = new System.Drawing.Point(12, 128);
            this.OK_spravichnik_button.Margin = new System.Windows.Forms.Padding(2);
            this.OK_spravichnik_button.Name = "OK_spravichnik_button";
            this.OK_spravichnik_button.Size = new System.Drawing.Size(84, 27);
            this.OK_spravichnik_button.TabIndex = 5;
            this.OK_spravichnik_button.Text = "OK";
            this.OK_spravichnik_button.UseVisualStyleBackColor = true;
            this.OK_spravichnik_button.Click += new System.EventHandler(this.OK_spravichnik_button_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(205, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(34, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "ФИО";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(208, 72);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(93, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Номер телефона";
            // 
            // SpravochnikForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(361, 166);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Cancel_spravichnik_button);
            this.Controls.Add(this.OK_spravichnik_button);
            this.Controls.Add(this.Number_comboBox);
            this.Controls.Add(this.Name_comboBox);
            this.Name = "SpravochnikForm";
            this.Text = "SpravochnikForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button Cancel_spravichnik_button;
        private System.Windows.Forms.Button OK_spravichnik_button;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        public System.Windows.Forms.ComboBox Name_comboBox;
        public System.Windows.Forms.ComboBox Number_comboBox;
    }
}